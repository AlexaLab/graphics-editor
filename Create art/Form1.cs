﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Create_art
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[] rb_Name = { "Карандаш", "Линия", "Веер", "Рамка", "Окружность", "Треугольник №1", "Треугольник №2", "Ломаная", "Безье", "Сплайн", "Прямоугольник", "Круг", "Ластик", "Текст", "Область", "Картинка" };
        int count_Mode;                 //Количество режимов
        RadioButton[] rb_Mas;           //Массив кнопок
        string mode = "Карандаш";       //Режим при запуске
        PictureBox pic_Object;
        Bitmap bit_Main, bit_Add;       //Основной битовый образ
        Graphics gr_Main, gr_Add;       //Графический инструмент для основного битового образа
        bool drag = false;              //Для перетаскивания с нажатой мышкой
        int x0, y0, x, y, w, h;         //Координаты и размеры

        //Настройки карандаша
        Pen pen;                        //Объявление карандаша
        Pen pen_Eraser;                 //Ластик
        Color color_Main = Color.Black; //Цвет карандаша
        Color color_Add = Color.Yellow; //Дополнительный цвет
        int pen_Thick = 3;              //Толщина карандаша

        //Настройки для фона
        Color back_Color = Color.White; //Цвет фона

        //Заливка
        string type_Brush = "Сплошная";       //Тип заливки
        SolidBrush solid_Brush;               //Создание сплошной заливки
        HatchBrush hatch_Brush;               //Создание узорной заливки
        HatchStyle hatch_Style = new HatchStyle();
        TextureBrush texture_Brush;           //Создание текстурной заливки
        string file_Name_Texture;             //Файл для текстурной заливки
        LinearGradientBrush gradient_Brush;   //Создание градиентной заливки
        Rectangle gradient_Rectangle = new Rectangle(0, 0, 50, 50);
        LinearGradientMode gradient_Mode = new LinearGradientMode(); 

        //Текст
        string text = "";
        Font text_Font = new Font("Segoe UI", 10);

        private void Form1_Load(object sender, EventArgs e)
        {
            count_Mode = rb_Name.Length;
            rb_Mas = new RadioButton[count_Mode];

            //Кнопки--------------------------------------------------------------------------------------
            for (int i = 0; i < count_Mode; i++)
            {
                rb_Mas[i] = new RadioButton();
                rb_Mas[i].Text = rb_Name[i];
                rb_Mas[i].Appearance = Appearance.Button;
                rb_Mas[i].FlatStyle = FlatStyle.Popup;
                rb_Mas[i].Checked = false;
                rb_Mas[i].Left = 10;
                rb_Mas[i].Top = 10 + i * rb_Mas[i].Height;
                rb_Mas[i].BackColor = Color.Ivory;
                rb_Mas[i].AutoCheck = true;
                rb_Mas[i].Click += radioButton_Click;
            }

            flowLayoutPanel1.Controls.AddRange(rb_Mas);
            rb_Mas[0].Checked = true;

            //Картинка-------------------------------------------------------------------------------------
            pic_Object = new PictureBox();
            pic_Object.Width = 104;
            pic_Object.Height = 104;
            pic_Object.Left = 10;
            pic_Object.Top = rb_Mas[rb_Mas.Length - 1].Top + rb_Mas[rb_Mas.Length - 1].Height + 5;
            pic_Object.BackColor = Color.FloralWhite;
            pic_Object.SizeMode = PictureBoxSizeMode.StretchImage;
            pic_Object.BorderStyle = BorderStyle.Fixed3D;
            flowLayoutPanel1.Controls.Add(pic_Object);

            //Строка состояния-----------------------------------------------------------------------------
            toolStripStatusLabel_mode.Text = mode;
            toolStripStatusLabel_coordinates.Text = "(0:0)";
            toolStripStatusLabel_date.Text = DateTime.Now.ToLongDateString();
            toolStripStatusLabel_time.Text = DateTime.Now.ToShortTimeString();


            //запуск таймера
            timer1.Enabled = true;

            //Подготовка фона------------------------------------------------------------------------------
            w = pictureBox_Main.Width;
            h = pictureBox_Main.Height;
            bit_Main = new Bitmap(w, h);
            gr_Main = Graphics.FromImage(bit_Main);
            gr_Main.Clear(back_Color);
            pictureBox_Main.Image = bit_Main;
            pictureBox_Main.Invalidate();

            //Создание дополнительного битмепа и графического инструмента
            bit_Add = new Bitmap(w, h);
            gr_Add = Graphics.FromImage(bit_Add);

            //Подготовка карандаша-------------------------------------------------------------------------
            pen = new Pen(color_Main, pen_Thick);
            pen.DashStyle = DashStyle.Solid;
            pen.StartCap = LineCap.Round;
            pen.EndCap = LineCap.Round;

            pen_Eraser = new Pen(back_Color, pen_Thick);
            pen_Eraser.DashStyle = DashStyle.Solid;
            pen_Eraser.StartCap = LineCap.Round;
            pen_Eraser.EndCap = LineCap.Round;

            //Диалоговые окна
            openFileDialog1.Filter = saveFileDialog1.Filter = "JPG|*.jpg|PNG|*.png";

            //Заливка
            toolStripComboBox_Style.Enabled = false;
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)  //Сохранение картинки
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                bit_Main.Save(saveFileDialog1.FileName);
                saveFileDialog1.OverwritePrompt = true;
                MessageBox.Show("Файл сохранён");
            }
        }

        private void toolStripButton_Open_Click(object sender, EventArgs e)  //Открывание картинки
        {
            Bitmap temp;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                temp = new Bitmap(openFileDialog1.FileName);
                gr_Main.DrawImage(temp, 0, 0);
                pictureBox_Main.Image = bit_Main;
                pictureBox_Main.Invalidate();
            }
        }

        private void toolStripButton_Clear_Click(object sender, EventArgs e)  //Очистка
        {
            if (this.colorDialog1.ShowDialog() == DialogResult.OK)
            {
                back_Color = this.colorDialog1.Color;
                gr_Main.Clear(back_Color);
                pictureBox_Main.Image = bit_Main;

                pen_Eraser.Color = back_Color;
            }
        }

        private void toolStripButton_Main_Color_Click(object sender, EventArgs e)  //Смена основного цвета
        {
            if (this.colorDialog1.ShowDialog() == DialogResult.OK)
            {
                color_Main = colorDialog1.Color;
                pen.Color = color_Main;
                toolStripButton_Main_Color.BackColor = color_Main;
            }
        }      

        private void toolStripButton_Add_Color_Click(object sender, EventArgs e)  //Смена дополнительного цвета
        {
            if (this.colorDialog1.ShowDialog() == DialogResult.OK)
            {
                color_Add = colorDialog1.Color;
                toolStripButton_Add_Color.BackColor = color_Add;
            }
        }

        private void toolStripComboBox_Thick_SelectedIndexChanged(object sender, EventArgs e)  //Смена толщины кисти
        {
            if (toolStripComboBox_Thick.Text == "")
            {
                pen_Thick = 1;
                return;
            }
            pen_Thick = int.Parse(toolStripComboBox_Thick.Text);
            pen.Width = pen_Thick;             //Толщина карандаша
            pen_Eraser.Width = pen_Thick;      //Толщина ластика
        }

        private void toolStripButton_Font_Click(object sender, EventArgs e)  //Смена шрифта
        {
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                //toolStripTextBox1.Font = fontDialog1.Font;
                //toolStripTextBox1.ForeColor = fontDialog1.Color;

                text_Font = fontDialog1.Font;
            }
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)  //Перехват нажатий клавиш клавиатуры
        {
            text = toolStripTextBox1.Text;
        }

        private void toolStripComboBox_Line_SelectedIndexChanged(object sender, EventArgs e)  //Смена типа линии
        {
            switch (toolStripComboBox_Line.Text)
            {
                case "Сплошная":
                    pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                    break;
                case "Пунктирная":
                    pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                    break;
                case "Пунктир с точкой":
                    pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
                    break;
                case "Пунктир с двумя точками":
                    pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDotDot;
                    break;
                case "Точки":
                    pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                    break;
            }
        }

        private void toolStripComboBox_Brush_SelectedIndexChanged(object sender, EventArgs e)   //Смена типа заливки
        {
            type_Brush = toolStripComboBox_Brush.Text;
            switch (type_Brush)
            {
                case "Сплошная":
                    toolStripComboBox_Style.Enabled = false;
                    toolStripComboBox_Style.Items.Clear();
                    break;
                case "Узорная":
                    toolStripComboBox_Style.Enabled = true;
                    toolStripComboBox_Style.Items.Clear();

                    for (int i = 0; i < Enum.GetValues(typeof(HatchStyle)).Length; i++)
                    {
                        toolStripComboBox_Style.Items.Add((HatchStyle)i).ToString();
                    }
                    toolStripComboBox_Style.Text = "Стиль заливки";
                    break;
                case "Текстурная":
                    toolStripComboBox_Style.Enabled = false;
                    toolStripComboBox_Style.Items.Clear();

                    if (openFileDialog1.ShowDialog() == DialogResult.OK) 
                    {
                        file_Name_Texture = openFileDialog1.FileName;
                    }
                    break;
                case "Градиентная":
                    toolStripComboBox_Style.Enabled = true;
                    toolStripComboBox_Style.Items.Clear();

                    for (int i = 0; i < Enum.GetValues(typeof(LinearGradientMode)).Length; i++)
                    {
                        toolStripComboBox_Style.Items.Add((LinearGradientMode)i).ToString();
                    }
                    toolStripComboBox_Style.Text = "Стиль заливки";
                    break;
            }
        }

        private void toolStripComboBox_Style_SelectedIndexChanged(object sender, EventArgs e)  //Смена стиля заливки
        {
            //hatch_Style = (HatchStyle)Enum.Parse(typeof(HatchStyle), toolStripComboBox_Style.SelectedItem.ToString(), true);
            //gradient_Mode = (LinearGradientMode)Enum.Parse(typeof(LinearGradientMode), toolStripComboBox_Style.SelectedItem.ToString(), true);
            gradient_Mode = (LinearGradientMode)toolStripComboBox_Style.SelectedItem;
            hatch_Style = (HatchStyle)toolStripComboBox_Style.SelectedItem;
        }

        private void pictureBox_Main_MouseDown(object sender, MouseEventArgs e)  //Кнопка мыши нажата
        {
            if (e.Button == MouseButtons.Left)
            {
                drag = true;  //Начать перетаскивание
                x0 = e.X;
                y0 = e.Y;
            }


            switch (mode)
            {
                case "Прямоугольник": case "Круг":     //Создание заливки
                    switch (type_Brush)
                    {
                        case "Сплошная":
                            solid_Brush = new SolidBrush(color_Add);
                            break;
                        case "Узорная":
                            hatch_Brush = new HatchBrush(hatch_Style, color_Add, color_Main);
                            break;
                        case "Текстурная":
                            Bitmap bit_Texure_Brush = new Bitmap(file_Name_Texture);
                            texture_Brush = new TextureBrush(bit_Texure_Brush);
                            break;
                        case "Градиентная":
                            //инициализация в MouseMove
                            break;
                    }
                    break;
            }

        }

        private void pictureBox_Main_MouseMove(object sender, MouseEventArgs e)  //Мышь двигается по главному пикчербоксу
        {
            //Отображение координат в строке состояния
            toolStripStatusLabel_coordinates.Text = $"({e.X}:{e.Y})";

            //Рисование------------------------------------------------------------------------------------------------
            if (drag)
            {
                x = e.X;
                y = e.Y;
                gr_Add.Clear(Color.Transparent);

                switch (mode)
                {
                    case "Карандаш":
                        gr_Main.DrawLine(pen, x0, y0, x, y);
                        x0 = x;
                        y0 = y;
                        break;
                    case "Линия":
                        gr_Add.DrawLine(pen, x0, y0, x, y);
                        break;
                    case "Веер":
                        gr_Main.DrawLine(pen, x0, y0, x, y);
                        break;
                    case "Рамка":
                        if (x > x0 && y > y0)
                        {
                            gr_Add.DrawRectangle(pen, x0, y0, x - x0, y - y0);
                        }
                        else if (x < x0 && y < y0)
                        {
                            gr_Add.DrawRectangle(pen, x, y, x0 - x, y0 - y);
                        }
                        else if (x > x0 && y < y0)
                        {
                            gr_Add.DrawRectangle(pen, x0, y, x - x0, y0 - y);
                        }
                        else if (x < x0 && y > y0)
                        {
                            gr_Add.DrawRectangle(pen, x, y0, x0 - x, y - y0);
                        }
                        break;
                    case "Окружность":
                        gr_Add.DrawEllipse(pen, x0, y0, x - x0, y - y0);
                        break;
                    case "Треугольник №1":
                        gr_Add.DrawLine(pen, x0, y0, x, y);
                        gr_Add.DrawLine(pen, x0, y0, x0 + (x0 - x), y);
                        gr_Add.DrawLine(pen, x, y, x0 + (x0 - x), y);
                        break;
                    case "Треугольник №2":
                        float y1, x1;
                        x1 = Convert.ToSingle((x - x0) * Math.Cos(-Math.PI / 3) - (y - y0) * Math.Sin(-Math.PI / 3) + x0);
                        y1 = Convert.ToSingle((x - x0) * Math.Sin(-Math.PI / 3) - (y - y0) * Math.Cos(-Math.PI / 3) + y0);
                        gr_Add.DrawLine(pen, x0, y0, x, y);
                        gr_Add.DrawLine(pen, x0, y0, x1, y1);
                        gr_Add.DrawLine(pen, x1, y1, x, y);
                        break;
                    case "Прямоугольник":
                        switch (type_Brush)
                        {
                            case "Сплошная":
                                if (x > x0 && y > y0)
                                {
                                    gr_Add.FillRectangle(solid_Brush, x0, y0, x - x0, y - y0);
                                    gr_Add.DrawRectangle(pen, x0, y0, x - x0, y - y0);
                                }
                                else if (x < x0 && y < y0)
                                {
                                    gr_Add.FillRectangle(solid_Brush, x, y, x0 - x, y0 - y);
                                    gr_Add.DrawRectangle(pen, x, y, x0 - x, y0 - y);
                                }
                                else if (x > x0 && y < y0)
                                {
                                    gr_Add.FillRectangle(solid_Brush, x0, y, x - x0, y0 - y);
                                    gr_Add.DrawRectangle(pen, x0, y, x - x0, y0 - y);
                                }
                                else if (x < x0 && y > y0)
                                {
                                    gr_Add.FillRectangle(solid_Brush, x, y0, x0 - x, y - y0);
                                    gr_Add.DrawRectangle(pen, x, y0, x0 - x, y - y0);
                                }
                                break;
                            case "Узорная":
                                if (x > x0 && y > y0)
                                {
                                    gr_Add.FillRectangle(hatch_Brush, x0, y0, x - x0, y - y0);
                                    gr_Add.DrawRectangle(pen, x0, y0, x - x0, y - y0);
                                }
                                else if (x < x0 && y < y0)
                                {
                                    gr_Add.FillRectangle(hatch_Brush, x, y, x0 - x, y0 - y);
                                    gr_Add.DrawRectangle(pen, x, y, x0 - x, y0 - y);
                                }
                                else if (x > x0 && y < y0)
                                {
                                    gr_Add.FillRectangle(hatch_Brush, x0, y, x - x0, y0 - y);
                                    gr_Add.DrawRectangle(pen, x0, y, x - x0, y0 - y);
                                }
                                else if (x < x0 && y > y0)
                                {
                                    gr_Add.FillRectangle(hatch_Brush, x, y0, x0 - x, y - y0);
                                    gr_Add.DrawRectangle(pen, x, y0, x0 - x, y - y0);
                                }
                                break;
                            case "Текстурная":
                                if (x > x0 && y > y0)
                                {
                                    gr_Add.FillRectangle(texture_Brush, x0, y0, x - x0, y - y0);
                                    gr_Add.DrawRectangle(pen, x0, y0, x - x0, y - y0);
                                }
                                else if (x < x0 && y < y0)
                                {
                                    gr_Add.FillRectangle(texture_Brush, x, y, x0 - x, y0 - y);
                                    gr_Add.DrawRectangle(pen, x, y, x0 - x, y0 - y);
                                }
                                else if (x > x0 && y < y0)
                                {
                                    gr_Add.FillRectangle(texture_Brush, x0, y, x - x0, y0 - y);
                                    gr_Add.DrawRectangle(pen, x0, y, x - x0, y0 - y);
                                }
                                else if (x < x0 && y > y0)
                                {
                                    gr_Add.FillRectangle(texture_Brush, x, y0, x0 - x, y - y0);
                                    gr_Add.DrawRectangle(pen, x, y0, x0 - x, y - y0);
                                }
                                break;
                            case "Градиентная":
                                gradient_Rectangle = new Rectangle(x0, y0, Math.Abs(x - x0) + 1, Math.Abs(y - y0) + 1);
                                gradient_Brush = new LinearGradientBrush(gradient_Rectangle, color_Main, color_Add, gradient_Mode);

                                if (x > x0 && y > y0)
                                {
                                    gr_Add.FillRectangle(gradient_Brush, x0, y0, x - x0, y - y0);
                                    gr_Add.DrawRectangle(pen, x0, y0, x - x0, y - y0);
                                }
                                else if (x < x0 && y < y0)
                                {
                                    gr_Add.FillRectangle(gradient_Brush, x, y, x0 - x, y0 - y);
                                    gr_Add.DrawRectangle(pen, x, y, x0 - x, y0 - y);
                                }
                                else if (x > x0 && y < y0)
                                {
                                    gr_Add.FillRectangle(gradient_Brush, x0, y, x - x0, y0 - y);
                                    gr_Add.DrawRectangle(pen, x0, y, x - x0, y0 - y);
                                }
                                else if (x < x0 && y > y0)
                                {
                                    gr_Add.FillRectangle(gradient_Brush, x, y0, x0 - x, y - y0);
                                    gr_Add.DrawRectangle(pen, x, y0, x0 - x, y - y0);
                                }
                                break;
                        }
                        break;
                    case "Круг":
                        switch (type_Brush)
                        {
                            case "Сплошная":
                                gr_Add.FillEllipse(solid_Brush, x0, y0, x - x0, y - y0);
                                break;
                            case "Узорная":
                                gr_Add.FillEllipse(hatch_Brush, x0, y0, x - x0, y - y0);

                                break;
                            case "Текстурная":
                                gr_Add.FillEllipse(texture_Brush, x0, y0, x - x0, y - y0);
                                break;
                            case "Градиентная":
                                gradient_Rectangle = new Rectangle(x0, y0, Math.Abs(x - x0) + 1, Math.Abs(y - y0) + 1);
                                gradient_Brush = new LinearGradientBrush(gradient_Rectangle, color_Main, color_Add, gradient_Mode);

                                gr_Add.FillEllipse(gradient_Brush, x0, y0, x - x0, y - y0);
                                break;
                        }
                        gr_Add.DrawEllipse(pen, x0, y0, x - x0, y - y0);
                        break;
                    case "Ластик":
                        gr_Main.DrawLine(pen_Eraser, x0, y0, x, y);
                        x0 = x;
                        y0 = y;
                        break;
                    case "Текст":
                        if (toolStripTextBox1.Text == "")
                        {
                            MessageBox.Show("Введите тест в поле в меню");
                            return;
                        }

                        gr_Add.DrawString(text, text_Font, solid_Brush, x, y);
                        break;

                }
                pictureBox_Main.Invalidate();
            }
        }

        private void pictureBox_Main_Paint(object sender, PaintEventArgs e)  //Перерисовка главного рикчербокса
        {
            e.Graphics.DrawImage(bit_Main, 0, 0);

            if (drag)
            {
                e.Graphics.DrawImage(bit_Add, 0, 0);
            }
        }

        private void pictureBox_Main_MouseUp(object sender, MouseEventArgs e)  //Кнопка мыши отпущена
        {            
            drag = false;                        //Закончить перетаскивание 

            gr_Main.DrawImage(bit_Add, 0, 0);    //Перенести битмап в другой битмап            
        }

        private void radioButton_Click(object sender, EventArgs e)  //Выбор радиобаттона
        {
            //Отображение режима в строке состояния
            RadioButton rb = sender as RadioButton;
            mode = rb.Text;
            toolStripStatusLabel_mode.Text = mode;
        }

        private void pictureBox_Main_MouseDoubleClick(object sender, MouseEventArgs e)  //Пипетка
        {
            x = e.X;
            y = e.Y;

            if (e.Button == MouseButtons.Left)
            {
                color_Main = bit_Main.GetPixel(x, y);
                pen.Color = color_Main;
                toolStripButton_Main_Color.BackColor = color_Main;
            }
            else
            {
                color_Add = bit_Main.GetPixel(x, y);
                toolStripButton_Add_Color.BackColor = color_Add;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)  //Отображение времени
        {
            toolStripStatusLabel_date.Text = DateTime.Now.ToLongDateString();
            toolStripStatusLabel_time.Text = DateTime.Now.ToShortTimeString();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            //bit_Main = new Bitmap(bit_Main, pictureBox_Main.Width, pictureBox_Main.Height);
            //pictureBox_Main.Image = bit_Main;
            //pictureBox_Main.Invalidate();
        }
    }
}
