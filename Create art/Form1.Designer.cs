﻿namespace Create_art
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Open = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Clear = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Main_Color = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Add_Color = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripComboBox_Thick = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButton_Font = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripComboBox_Line = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripComboBox_Brush = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox_Style = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel_mode = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_coordinates = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_date = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_time = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox_Main = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Main)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_Save,
            this.toolStripButton_Open,
            this.toolStripSeparator1,
            this.toolStripButton_Clear,
            this.toolStripButton_Main_Color,
            this.toolStripButton_Add_Color,
            this.toolStripSeparator2,
            this.toolStripComboBox_Thick,
            this.toolStripButton_Font,
            this.toolStripTextBox1,
            this.toolStripComboBox_Line,
            this.toolStripSeparator3,
            this.toolStripComboBox_Brush,
            this.toolStripComboBox_Style,
            this.toolStripButton_Exit});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1064, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Save.Text = "toolStripButton1";
            this.toolStripButton_Save.ToolTipText = "Сохранить";
            this.toolStripButton_Save.Click += new System.EventHandler(this.toolStripButton_Save_Click);
            // 
            // toolStripButton_Open
            // 
            this.toolStripButton_Open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Open.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Open.Image")));
            this.toolStripButton_Open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Open.Name = "toolStripButton_Open";
            this.toolStripButton_Open.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Open.Text = "toolStripButton2";
            this.toolStripButton_Open.ToolTipText = "Открыть";
            this.toolStripButton_Open.Click += new System.EventHandler(this.toolStripButton_Open_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_Clear
            // 
            this.toolStripButton_Clear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Clear.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Clear.Image")));
            this.toolStripButton_Clear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Clear.Name = "toolStripButton_Clear";
            this.toolStripButton_Clear.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Clear.Text = "toolStripButton3";
            this.toolStripButton_Clear.ToolTipText = "Очистить";
            this.toolStripButton_Clear.Click += new System.EventHandler(this.toolStripButton_Clear_Click);
            // 
            // toolStripButton_Main_Color
            // 
            this.toolStripButton_Main_Color.BackColor = System.Drawing.Color.Black;
            this.toolStripButton_Main_Color.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Main_Color.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Main_Color.Name = "toolStripButton_Main_Color";
            this.toolStripButton_Main_Color.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Main_Color.Text = "toolStripButton4";
            this.toolStripButton_Main_Color.ToolTipText = "Основной цвет";
            this.toolStripButton_Main_Color.Click += new System.EventHandler(this.toolStripButton_Main_Color_Click);
            // 
            // toolStripButton_Add_Color
            // 
            this.toolStripButton_Add_Color.BackColor = System.Drawing.Color.Yellow;
            this.toolStripButton_Add_Color.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Add_Color.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Add_Color.Name = "toolStripButton_Add_Color";
            this.toolStripButton_Add_Color.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Add_Color.Text = "toolStripButton5";
            this.toolStripButton_Add_Color.ToolTipText = "Дополнительный цвет";
            this.toolStripButton_Add_Color.Click += new System.EventHandler(this.toolStripButton_Add_Color_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripComboBox_Thick
            // 
            this.toolStripComboBox_Thick.AutoSize = false;
            this.toolStripComboBox_Thick.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripComboBox_Thick.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.toolStripComboBox_Thick.MaxDropDownItems = 10;
            this.toolStripComboBox_Thick.Name = "toolStripComboBox_Thick";
            this.toolStripComboBox_Thick.Size = new System.Drawing.Size(50, 25);
            this.toolStripComboBox_Thick.Text = "3";
            this.toolStripComboBox_Thick.ToolTipText = "Толщина кисти";
            this.toolStripComboBox_Thick.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox_Thick_SelectedIndexChanged);
            this.toolStripComboBox_Thick.TextChanged += new System.EventHandler(this.toolStripComboBox_Thick_SelectedIndexChanged);
            // 
            // toolStripButton_Font
            // 
            this.toolStripButton_Font.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Font.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Font.Image")));
            this.toolStripButton_Font.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Font.Name = "toolStripButton_Font";
            this.toolStripButton_Font.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Font.Text = "toolStripButton7";
            this.toolStripButton_Font.ToolTipText = "Шрифт";
            this.toolStripButton_Font.Click += new System.EventHandler(this.toolStripButton_Font_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(300, 25);
            this.toolStripTextBox1.ToolTipText = "Ввод текста";
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            // 
            // toolStripComboBox_Line
            // 
            this.toolStripComboBox_Line.AutoSize = false;
            this.toolStripComboBox_Line.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripComboBox_Line.Items.AddRange(new object[] {
            "Сплошная",
            "Пунктирная",
            "Пунктир с точкой",
            "Пунктир с двумя точками",
            "Точки"});
            this.toolStripComboBox_Line.Name = "toolStripComboBox_Line";
            this.toolStripComboBox_Line.Size = new System.Drawing.Size(160, 25);
            this.toolStripComboBox_Line.Text = "Кисть";
            this.toolStripComboBox_Line.ToolTipText = "Тип кисти";
            this.toolStripComboBox_Line.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox_Line_SelectedIndexChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripComboBox_Brush
            // 
            this.toolStripComboBox_Brush.AutoSize = false;
            this.toolStripComboBox_Brush.Items.AddRange(new object[] {
            "Сплошная",
            "Узорная",
            "Текстурная",
            "Градиентная"});
            this.toolStripComboBox_Brush.Name = "toolStripComboBox_Brush";
            this.toolStripComboBox_Brush.Size = new System.Drawing.Size(90, 23);
            this.toolStripComboBox_Brush.Text = "Заливка";
            this.toolStripComboBox_Brush.ToolTipText = "Тип заливки";
            this.toolStripComboBox_Brush.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox_Brush_SelectedIndexChanged);
            // 
            // toolStripComboBox_Style
            // 
            this.toolStripComboBox_Style.Name = "toolStripComboBox_Style";
            this.toolStripComboBox_Style.Size = new System.Drawing.Size(150, 25);
            this.toolStripComboBox_Style.ToolTipText = "Стиль заливки";
            this.toolStripComboBox_Style.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox_Style_SelectedIndexChanged);
            // 
            // toolStripButton_Exit
            // 
            this.toolStripButton_Exit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Exit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Exit.Image")));
            this.toolStripButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Exit.Name = "toolStripButton_Exit";
            this.toolStripButton_Exit.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Exit.Text = "toolStripButton6";
            this.toolStripButton_Exit.ToolTipText = "Выход";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel_mode,
            this.toolStripStatusLabel_coordinates,
            this.toolStripStatusLabel_date,
            this.toolStripStatusLabel_time});
            this.statusStrip1.Location = new System.Drawing.Point(0, 657);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1064, 24);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel_mode
            // 
            this.toolStripStatusLabel_mode.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripStatusLabel_mode.Name = "toolStripStatusLabel_mode";
            this.toolStripStatusLabel_mode.Size = new System.Drawing.Size(52, 19);
            this.toolStripStatusLabel_mode.Text = "Режим";
            // 
            // toolStripStatusLabel_coordinates
            // 
            this.toolStripStatusLabel_coordinates.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripStatusLabel_coordinates.Name = "toolStripStatusLabel_coordinates";
            this.toolStripStatusLabel_coordinates.Size = new System.Drawing.Size(88, 19);
            this.toolStripStatusLabel_coordinates.Text = "Координаты";
            // 
            // toolStripStatusLabel_date
            // 
            this.toolStripStatusLabel_date.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripStatusLabel_date.Name = "toolStripStatusLabel_date";
            this.toolStripStatusLabel_date.Size = new System.Drawing.Size(39, 19);
            this.toolStripStatusLabel_date.Text = "Дата";
            // 
            // toolStripStatusLabel_time
            // 
            this.toolStripStatusLabel_time.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripStatusLabel_time.Name = "toolStripStatusLabel_time";
            this.toolStripStatusLabel_time.Size = new System.Drawing.Size(49, 19);
            this.toolStripStatusLabel_time.Text = "Время";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 28);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(108, 628);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox_Main
            // 
            this.pictureBox_Main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_Main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.pictureBox_Main.Location = new System.Drawing.Point(114, 30);
            this.pictureBox_Main.Name = "pictureBox_Main";
            this.pictureBox_Main.Size = new System.Drawing.Size(950, 624);
            this.pictureBox_Main.TabIndex = 3;
            this.pictureBox_Main.TabStop = false;
            this.pictureBox_Main.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Main_Paint);
            this.pictureBox_Main.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Main_MouseDoubleClick);
            this.pictureBox_Main.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Main_MouseDown);
            this.pictureBox_Main.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Main_MouseMove);
            this.pictureBox_Main.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Main_MouseUp);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.pictureBox_Main);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Create art";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Main)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripButton toolStripButton_Open;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Clear;
        private System.Windows.Forms.ToolStripButton toolStripButton_Main_Color;
        private System.Windows.Forms.ToolStripButton toolStripButton_Add_Color;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox_Thick;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Exit;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox_Line;
        private System.Windows.Forms.ToolStripButton toolStripButton_Font;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_mode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_coordinates;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_date;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_time;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox_Main;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox_Brush;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox_Style;
        private System.Windows.Forms.FontDialog fontDialog1;
    }
}

